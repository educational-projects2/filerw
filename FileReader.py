import json
import xml.etree.ElementTree as ET
import os


def simple_txt_reader():
    if os.path.isfile('files/file.txt'):
        f = open('files/file.txt', 'r')
        return f.read()
    else:
        return 'Файл files/file.txt не существует'

#print(simple_txt_reader())


def json_reader():
    with open('files/file.json', 'r', encoding='utf-16') as file:
        data = json.load(file)
        return data


#json_reader()


def xml_reader():
    data = ET.parse('files/file.xml')
    #students = data.getroot()
    #for elem in students:
        #for subelem in elem:
            #print(subelem.text)
    return data



#xml_reader()



