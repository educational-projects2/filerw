from tkinter import *
import os
import FileReader
import FileWriter
import FilesystemInfo


def readTxt():
    output.configure(text=FileReader.simple_txt_reader())
def writeTxt():
    FileWriter.simple_txt_writer(txtEntry.get())
def writeJson():
    FileWriter.json_writer(jsonSurnameEntry.get(), jsonNameEntry.get(), jsonGroupEntry.get())
def readJson():
    if os.path.isfile('files/file.json'):
        data = '{\'students\':[\n'
        for student in FileReader.json_reader()['students']:
            data += '{\'surname\': ' + student['surname'] + ', \'name\': ' + student['name'] + ', \'group\': ' + student['group'] + '} \n'
        data += ' ]}'
        output.configure(text=data)
    else:
        output.configure(text='Файл files/file.json не существует')
def writeXml():
    FileWriter.xml_writer(xmlSurnameEntry.get(), xmlNameEntry.get(), xmlGroupEntry.get())
def readXml():
    if os.path.isfile('files/file.xml'):
        data = 'Students\n'
        students = FileReader.xml_reader().getroot()
        count = 1
        for elem in students:
            data += 'Студент №'+str(count)+': \n'
            for subelem in elem:
                data += subelem.text+'\n'
            count += 1
        output.configure(text=data)
    else:
        output.configure(text='Файл files/file.xml не существует')
def packZip():
    FileWriter.zip_writer()
def systemInfo():
    data = 'Доступные диски: ' + str(FilesystemInfo.get_drivers()) + '\n'
    data += FilesystemInfo.get_drivers_info()
    output.configure(text=data)
def clicked():
    print('пустышка')


window = Tk()
window.title("Добро пожаловать в приложение FileWriterReader")
window.geometry('400x500')

txtLbl = Label(window, text=".txt")
txtLbl.grid(column=0, row=0)
txtContentLbl = Label(window, text="Содержание")
txtContentLbl.grid(column=1, row=0)
txtEntry = StringVar() #--------------заветное получение значения из Entry
txt = Entry(window, width=30, textvariable=txtEntry)
txt.grid(column=2, row=0)
btnTxtWrite = Button(window, text="Пиши", command=writeTxt)
btnTxtWrite.grid(column=3, row=1)
btnTxtRead = Button(window, text="Читай", command=readTxt)
btnTxtRead.grid(column=4, row=1)

jsonLbl = Label(window, text=".json")
jsonLbl.grid(column=0, row=2)
jsonSurnameLbl = Label(window, text="Фамилия")
jsonSurnameLbl.grid(column=1, row=2)
jsonSurnameEntry = StringVar()
jsonSurname = Entry(window, width=30, textvariable=jsonSurnameEntry)
jsonSurname.grid(column=2, row=2)
jsonSurnameLbl = Label(window, text="Имя")
jsonSurnameLbl.grid(column=1, row=3)
jsonNameEntry = StringVar()
jsonName = Entry(window, width=30, textvariable=jsonNameEntry)
jsonName.grid(column=2, row=3)
jsonGroupLbl = Label(window, text="Группа")
jsonGroupLbl.grid(column=1, row=4)
jsonGroupEntry = StringVar()
jsonGroup = Entry(window, width=30, textvariable=jsonGroupEntry)
jsonGroup.grid(column=2, row=4)
btnJsonWrite = Button(window, text="Пиши", command=writeJson)
btnJsonWrite.grid(column=3, row=5)
btnJsonRead = Button(window, text="Читай", command=readJson)
btnJsonRead.grid(column=4, row=5)

xmlLbl = Label(window, text=".xml")
xmlLbl.grid(column=0, row=6)
xmlSurnameLbl = Label(window, text="Фамилия")
xmlSurnameLbl.grid(column=1, row=6)
xmlSurnameEntry = StringVar()
xmlSurname = Entry(window, width=30, textvariable=xmlSurnameEntry)
xmlSurname.grid(column=2, row=6)
xmlSurnameLbl = Label(window, text="Имя")
xmlSurnameLbl.grid(column=1, row=7)
xmlNameEntry = StringVar()
xmlName = Entry(window, width=30, textvariable=xmlNameEntry)
xmlName.grid(column=2, row=7)
xmlGroupLbl = Label(window, text="Группа")
xmlGroupLbl.grid(column=1, row=8)
xmlGroupEntry = StringVar()
xmlGroup = Entry(window, width=30, textvariable=xmlGroupEntry)
xmlGroup.grid(column=2, row=8)
btnxmlWrite = Button(window, text="Пиши", command=writeXml)
btnxmlWrite.grid(column=3, row=9)
btnxmlRead = Button(window, text="Читай", command=readXml)
btnxmlRead.grid(column=4, row=9)

xmlLbl = Label(window, text="Пакуем в .zip")
xmlLbl.grid(column=2, row=10)
btnxmlRead = Button(window, text="Pack", command=packZip)
btnxmlRead.grid(column=3, row=10)

xmlLbl = Label(window, text="Хочу информацию о дисках!!!")
xmlLbl.grid(column=2, row=12)
btnxmlRead = Button(window, text="Смотреть", command=systemInfo)
btnxmlRead.grid(column=3, row=12)

output = Label(window)
output.grid(column=2, row=15)

window.mainloop()
