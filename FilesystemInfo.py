import os, string
import psutil


def get_drivers():
    available_drives = ['%s:' % d for d in string.ascii_uppercase if os.path.exists('%s:' % d)]
    return available_drives


def get_drivers_info():
    d = psutil.disk_partitions()
    drivers_info = ''
    print('B диск информация:', d[0])
    drivers_info += 'B диск информация:' + str(d[0]) + '\n'
    print('C информация о диске:', d[1])
    drivers_info += 'C информация о диске:' + str(d[1]) + '\n'
    print('D информация о диске:', d[2])
    drivers_info += 'D информация о диске:' + str(d[2]) + '\n'
    print('Получить названия дисков:', d[0][0], d[1][0], d[2][0])
    drivers_info += 'Названия дисков:' + str(d[0][0]) + str(d[1][0]) + str(d[2][0]) + '\n'

    p = psutil.disk_usage(d[0][0]) #B disk
    print('процент использования диска B: ', p)
    drivers_info += 'процент использования диска B: ' + str(p) + '\n'
    p = psutil.disk_usage(d[1][0]) #C disk
    print('процент использования диска C: ', p)
    drivers_info += 'процент использования диска C: ' + str(p) + '\n'
    p = psutil.disk_usage(d[2][0]) #D disk
    print('D процент использования диска:', p)
    drivers_info += 'D процент использования диска:' + str(p) + '\n'

    return drivers_info
